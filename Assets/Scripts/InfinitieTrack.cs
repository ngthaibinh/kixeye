﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinitieTrack : Singleton<InfinitieTrack> {

    public float spawnCoinRate = 1.5f;
    public Transform lastPlatform;
    public GameObject platformPref;
    public Transform bonusCoin;
    public BallController ballController;
    public Material colorMat;
    public float colorSpeed = 2;
    public Color formColor;
    public Color toColor;
    public ParticleSystem bonusFx;
    public int obstacleScore = 10;
    public int coinScore =10;

    private GameObject platformPools;
    private List<Obstacle> platforms = new List<Obstacle>();
    private int pIndex;
    private WaitForSeconds _spawnCoinRate;
    private int playerScore;
    private Vector2 startPosition = new Vector2(0,0);
    private Vector2 startStarPosition = new Vector2(6.46f, 2.83f);
        
    private void Start() {
        //Create platform pool
        this.playerScore = 0;
        this.platformPools = new GameObject("PlatformPools");
        this.platformPools.transform.position = new Vector2(0, -7.1f);
        for (int i = 0; i < 5; i++)
        {
            this.platforms.Add(Instantiate(this.platformPref, this.platformPools.transform).GetComponent<Obstacle>());
            PostionPlatformByIndex(i);
        }
        this.pIndex = -1;
        //End
        //Setup spawn bonus coin routine
        this._spawnCoinRate = new WaitForSeconds(this.spawnCoinRate);
        StartCoroutine(SpawnBonusCoin());
    }

    private void Update()
    {
        //I love to watch the color game so I make a change on it.
        float lerp = Mathf.PingPong(Time.time, this.colorSpeed) / this.colorSpeed;
        Color c = Color.Lerp(this.formColor, this.toColor, lerp);
        this.colorMat.color = c;
    }

    /// <summary>
    /// Event OnEndPointReach when the ball reach the end point trigger
    /// </summary>
    public void OnEndPointReach()
    {
        if (this.pIndex >= 0 && this.pIndex < this.platforms.Count)
        {
            PostionPlatformByIndex(this.pIndex);
        }
        this.pIndex = this.pIndex == (this.platforms.Count - 1) ? 0 : (this.pIndex + 1);
    }

    /// <summary>
    /// Increase player score when player get a bonus coin
    /// </summary>
    public void OnHitBonusCoin()
    {
        this.bonusCoin.gameObject.SetActive(false);
        this.bonusFx.transform.position = this.bonusCoin.position;
        this.bonusFx.Play();
        this.playerScore += this.coinScore;
        UIManager.Instance.UpdateScore(this.playerScore);
    }

    /// <summary>
    /// Increase player score when player get over a obstacle
    /// </summary>
    public void OnPassObstacle()
    {
        this.playerScore += this.obstacleScore;
        UIManager.Instance.UpdateScore(this.playerScore);
    }

    private IEnumerator SpawnBonusCoin()
    {
        while (this.ballController.state != BallController.ActionState.DEAD)
        {
            yield return this._spawnCoinRate;
            Vector2 pos = this.ballController.transform.position;
            pos.x += 30;
            pos.y = 2.5f;
            this.bonusCoin.position = pos;
            this.bonusCoin.gameObject.SetActive(true);
        }
    }

    public int GetPlayerScore()
    {
        return playerScore;
    }

    /// <summary>
    /// Reset all platforms when game reset
    /// </summary>
    public void ResetPlatform()
    {
        StopAllCoroutines();
        this.playerScore = 0;
        this.lastPlatform.position = this.startPosition;
        for (int i = 0; i < 5; i++)
        {
            PostionPlatformByIndex(i);
        }
        this.pIndex = -1;
        this.bonusCoin.position = this.startStarPosition;
        this.bonusCoin.gameObject.SetActive(true);
        StartCoroutine(SpawnBonusCoin());
    }

    /// <summary>
    /// Rearrane a platform to new position base on index and lastplatform position
    /// </summary>
    /// <param name="index"></param>
    private void PostionPlatformByIndex(int index)
    {
        Vector2 pos = new Vector2(this.lastPlatform.position.x + 30, 0);
        this.platforms[index].transform.localPosition = pos;
        this.lastPlatform = this.platforms[index].transform;
        this.platforms[index].RandomNewObstacle();
    }

}
