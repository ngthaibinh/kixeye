﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {

    public Text txtScore, txtFinalScore;
    public CanvasGroup pauseScreen;
    [HideInInspector]
    public Animator uiAnim;

    private void Start()
    {
        this.uiAnim = GetComponent<Animator>();
    }

    public void UpdateScore(int playerScore)
    {
        this.txtScore.text = playerScore.ToString();
        this.uiAnim.SetTrigger("score");
    }

    public void SetAnimatorReady()
    {
        this.uiAnim.SetBool("ready", true);
    }

    public void UpdateFinalScore()
    {
        this.txtFinalScore.text = "Final Score: " + this.txtScore.text;
        this.txtScore.text = "00";
    }

    public void TogglePauseScreen()
    {
        if(this.pauseScreen.alpha == 1)
        {
            GameManager.Instance.UnPause();
            this.pauseScreen.alpha = 0;
            this.pauseScreen.blocksRaycasts = false;
        }
        else
        {
            GameManager.Instance.Pause();
            this.pauseScreen.alpha = 1;
            this.pauseScreen.blocksRaycasts = true;
        }
    }

}
