﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public GameObject[] obstacle_1;

    private int lastIndex;
    
    /// <summary>
    /// To random a new type of obstacles in obstacle_1
    /// </summary>
    public void RandomNewObstacle()
    {
        //Unactive the last obstacle to random a new one
        this.obstacle_1[this.lastIndex].SetActive(false);
        this.lastIndex = Random.Range(0, this.obstacle_1.Length);
        this.obstacle_1[this.lastIndex].SetActive(true);
    }
}
