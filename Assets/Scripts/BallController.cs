﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class BallController : MonoBehaviour {
    //For some reason Vector2.down doesn't exist in most Unity versions
    static Vector2 _v2down = new Vector2(0, -1);
    //For some reason Vector2.left doesn't exist in most Unity versions
    static Vector2 _v2left = new Vector2(-1, 0);

    public enum ActionState { RUN, JUMP, FALL, DEAD }

    public ActionState state
    {
        get
        {
            return _state;
        }
        set
        {
            if (_state != value)
            {
                _state = value;
            }
        }
    }
    private ActionState _state;
    
    [Header("Raycasting")]
    public LayerMask groundMask;
    public LayerMask obstacleMask;

    [Header("Speeds & Timings")]
    public float runSpeed = 5;
    public float jumpSpeed = 12;
    /// <summary>
    /// How long you can hold onto the jump button to increase jump height
    /// </summary>
    public float jumpDuration = 0.5f;

    [Header("Physics")]
    /// <summary>
    /// Additional fall gravity to feel more platformy
    /// </summary>
    public float fallGravity = -4;   

    [Header("References")]
    public CircleCollider2D primaryCollider;
    public SpriteRenderer model;
    public GameObject deadFx;

    //Afer fall injection
    public System.Action<BallController> OnGroundAfterFall;
    public string jumpSound;

    private Rigidbody2D rb;
    private PhysicsMaterial2D characterColliderMaterial;
    private bool doJump;
    private bool jumpPressed;
    private float jumpStartTime;    
    private Vector3 centerGroundCastOrigin;
    private float savedXVelocity;
    private float groundCastLength;
    private float backCastLength;

    private void Start () {
        this.rb = GetComponent<Rigidbody2D>();
        
        CalculateRayBounds(this.primaryCollider);

        if (this.primaryCollider.sharedMaterial == null)
            this.characterColliderMaterial = new PhysicsMaterial2D("CharacterColliderMaterial");
        else
            this.characterColliderMaterial = Instantiate(this.primaryCollider.sharedMaterial);

        this.primaryCollider.sharedMaterial = this.characterColliderMaterial;
        this.characterColliderMaterial.friction = 0;
        
        this.groundCastLength = this.primaryCollider.radius + 0.25f;
        this.backCastLength = this.groundCastLength + 15;
    }

    //Calculate where the collision rays should be based on a polygon collider
    private void CalculateRayBounds(CircleCollider2D coll)
    {
        Bounds b = coll.bounds;
        Vector3 center = transform.InverseTransformPoint(b.center);
        
        this.centerGroundCastOrigin.x = center.x;
        this.centerGroundCastOrigin.y = center.y;        
    }

    //Physics loop
    private void FixedUpdate()
    {
        HandlePhysics();
    }

    public void Input(bool JUMP_isPressed, bool JUMP_wasPressed)
    {
        if (((this.OnGround) && this.state < ActionState.JUMP))
        {
            if (!this.jumpPressed)
            {
                this.doJump = JUMP_wasPressed;
                if (this.doJump)
                {
                    this.jumpPressed = true;
                }
            }
        }
        this.jumpPressed = JUMP_isPressed;
    }

    private bool OnGround
    {
        get
        {
            return GroundCast();
        }
    }

    //See if character is on the ground
    private bool GroundCast()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.TransformPoint(this.centerGroundCastOrigin), _v2down, this.groundCastLength, this.groundMask);
        if (hit.collider != null && !hit.collider.isTrigger)
        {
            if (hit.normal.y < 0.4f)
                return false;            
            return true;
        }

        return false;
    }

    //See behind character after fall
    public bool ObstacleCast()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.TransformPoint(this.centerGroundCastOrigin), _v2left, this.backCastLength, this.obstacleMask);
        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }

    //Cache x velocity to ensure speed restores after heavy impact that results in physics penalties
    private void HandlePhysics()
    {
        //Auto run
        float x = 1;
        
        float xVelocity = 0;
        Vector2 velocity = this.rb.velocity;
        
        if (this.doJump && this.state < ActionState.JUMP)
        {
            //TODO: Impleament jump shound here   
            velocity.y = this.jumpSpeed;
            this.jumpStartTime = Time.time;
            this.state = ActionState.JUMP;
            this.doJump = false;
            
        }
        
        //Ground logic
        if (this.state < ActionState.JUMP)
        {
            if (this.OnGround)
            {
                xVelocity = this.runSpeed * Mathf.Sign(x);
                velocity.x = Mathf.MoveTowards(velocity.x, xVelocity, Time.deltaTime * 15);
                this.state = ActionState.RUN;
            }
            else
            {
                this.state = ActionState.FALL;
            }
            //Air logic
        }
        else if (this.state == ActionState.JUMP)
        {
            float jumpTime = Time.time - this.jumpStartTime;
            this.savedXVelocity = velocity.x;
            if (!this.jumpPressed || jumpTime >= this.jumpDuration)
            {
                this.jumpStartTime -= this.jumpDuration;

                if (velocity.y > 0)
                    velocity.y = Mathf.MoveTowards(velocity.y, 0, Time.deltaTime * 30);

                if (velocity.y <= 0 || (jumpTime < this.jumpDuration && this.OnGround))
                {
                    this.state = ActionState.FALL;
                }
            }
            //Fall logic
        }
        else if (this.state == ActionState.FALL)
        {
            if (this.OnGround)
            {
                //keep player running
                velocity.x = this.savedXVelocity;
                this.state = ActionState.RUN;                
                if (ObstacleCast())
                {
                    
                    if (OnGroundAfterFall != null)
                        OnGroundAfterFall(this);
                }
            }
        }
        
        //Falling
        if (this.state == ActionState.FALL)
            velocity.y += this.fallGravity * Time.deltaTime;
        //Dying
        if (this.state == ActionState.DEAD)
        {
            velocity = Vector2.zero;
        }
        this.rb.velocity = velocity;
    }
    
    public void Dead()
    {
        this.state = ActionState.DEAD;
        this.model.enabled = false;
        this.deadFx.SetActive(true);        
    }

    public void Revie()
    {
        this.state = ActionState.RUN;
        this.model.enabled = true;
        this.deadFx.SetActive(false);
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Handles.Label(transform.position, state.ToString());
        if (!Application.isPlaying)
            return;

        if (OnGround)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.grey;

        Gizmos.DrawWireCube(transform.position, new Vector3(0.5f, 0.5f, 0.5f));

        Gizmos.DrawWireSphere(transform.TransformPoint(this.centerGroundCastOrigin), 0.07f);

        Gizmos.DrawLine(transform.TransformPoint(this.centerGroundCastOrigin), transform.TransformPoint(this.centerGroundCastOrigin + new Vector3(-this.groundCastLength, 0, 0)));
        
    }
#endif

}
