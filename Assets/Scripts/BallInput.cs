﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;

public class BallInput : MonoBehaviour {

    public PressGesture presGesture;
    public ReleaseGesture releaseGesture;
    public bool isDieOnCollide = true;

    private BallController ballController;
    
    void Awake()
    {
        ballController = GetComponent<BallController>();
    }

    private void OnEnable()
    {
        this.presGesture.Pressed += PressHandler;
        this.releaseGesture.Released += ReleaseHandler;
        this.ballController.OnGroundAfterFall += OnGroundAfterFall;
    }

    private void OnDisable()
    {
        this.presGesture.Pressed -= PressHandler;
        this.releaseGesture.Released -= ReleaseHandler;
        this.ballController.OnGroundAfterFall -= OnGroundAfterFall;
    }

    void PressHandler(object sender, System.EventArgs e)
    {
        this.ballController.Input(true, true);
    }

    void ReleaseHandler(object sender, System.EventArgs e)
    {
        this.ballController.Input(false, false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.name)
        {
            case "EP":
                InfinitieTrack.Instance.OnEndPointReach();
                break;
            case "BonusStar":
                InfinitieTrack.Instance.OnHitBonusCoin();
                break;
            case "Brick":
                if (this.isDieOnCollide)
                    Dead();
                break;
        }

    }

    void OnGroundAfterFall(BallController controller)
    {
        InfinitieTrack.Instance.OnPassObstacle();
    }

    void Dead()
    {        
        GameManager.Instance.StartGameOver();
    }

}
