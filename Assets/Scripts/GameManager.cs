﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : Singleton<GameManager> {
    
    public BallController ballController;
    public BallInput ballInput;

    private Vector3 ballStartPosition;

    private IEnumerator Start()
    {
        this.ballStartPosition = this.ballInput.transform.position;
        this.ballInput.enabled = false;
        this.ballController.runSpeed /= 2;
        yield return new WaitForSeconds(3f);
        this.ballController.Input(true, true);
        UIManager.Instance.uiAnim.SetTrigger("start");        
        yield return new WaitForSeconds(1);
        this.ballInput.enabled = true ;
        this.ballController.runSpeed *= 2;
        Pause();
    }

    public void Pause()
    {
        Time.timeScale = 0;        
    }

    public void UnPause()
    {
        Time.timeScale = 2;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void StartGameOver()
    {
        StartCoroutine(GameOver());        
    }

    private IEnumerator GameOver()
    {
        this.ballController.Dead();
        this.ballInput.enabled = false;
        FollowCamera.Instance.Shake();
        yield return new WaitForSeconds(1);
        UIManager.Instance.UpdateFinalScore();
        UIManager.Instance.uiAnim.SetTrigger("gameover");
        yield return new WaitForSeconds(1);
        UIManager.Instance.uiAnim.SetBool("ready", false);
        Pause();
        StartCoroutine(PostScore());
    }

    public void StartReSetGame()
    {
        StartCoroutine(ResetGame());
    }

    private IEnumerator ResetGame()
    {
        this.ballController.Revie();
        InfinitieTrack.Instance.ResetPlatform();
        FollowCamera.Instance.ResetVision();
        this.ballInput.transform.position = this.ballStartPosition;        
        this.ballController.runSpeed /= 2;
        UnPause();
        yield return new WaitForSeconds(2.5f);
        this.ballController.Input(true, true);
        UIManager.Instance.uiAnim.SetTrigger("start");
        yield return new WaitForSeconds(1);
        this.ballInput.enabled = true;
        this.ballController.runSpeed *= 2;
        Pause();
    }

    private IEnumerator PostScore()
    {
        int playerScore = InfinitieTrack.Instance.GetPlayerScore();
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        // I want to add more security with md5 key.
        formData.Add(new MultipartFormDataSection("userName=binhnguyen&score=" + playerScore + "&key=securitywithmd5"));

        UnityWebRequest www = UnityWebRequest.Post("http://www.kixeyeserver.com/leaderboard", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            switch (www.responseCode)
            {
                case 200:
                    Debug.Log("Post Player Score successful");
                    break;
                case 404:
                    Debug.Log("Username not found (user has not registered with the leaderboard service)");
                    break;
                case 405:
                    Debug.Log("Invalid Username supplied");
                    break;
            }
            
        }
    }

}
