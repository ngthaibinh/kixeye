﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowCamera : Singleton<FollowCamera> {
	
	public Transform target;
	public Vector3 offset;
    public Color skyColor_1 = Color.red;
    public Color skyColor_2 = Color.blue;
    public float duration = 20;
    private Rigidbody2D rb;
    private Camera cam;
    private Animator anim;


	void Start () {
        this.anim = GetComponent<Animator>();
        this.rb = target.GetComponent<Rigidbody2D>();
        this.cam = GetComponent<Camera>();
        this.cam.clearFlags = CameraClearFlags.SolidColor;
    }

	Vector3 GetGoalPosition () {
		Vector3 goalPos = this.target.position + this.offset;
        Vector3 velocity = this.rb.velocity;
            
        goalPos += velocity;
        goalPos.y = 0;
        goalPos.z = -10;
        return goalPos;
	}

	void LateUpdate () {		
		Vector3 goalPos = Vector3.zero;
        goalPos = GetGoalPosition();
        transform.position = Vector3.Lerp(transform.position, goalPos, 1 * Time.deltaTime);
        float t = Mathf.PingPong(Time.time, this.duration) / this.duration;
        this.cam.backgroundColor = Color.Lerp(this.skyColor_1, this.skyColor_2, t);
    }

    public void ResetVision()
    {
        transform.position = Vector3.zero;
    }

    public void Shake()
    {
        this.anim.SetTrigger("shake");
    }

}
