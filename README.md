# Project name: BallChallenge

# My own Goals:

* Finish winthin 24 hour with 6 hours for sleep.
* A completed game.

# Reference Assets used:

* TouchScript, for reason to get input by Press/Release on the phone screen easier and can testing on editor faster with mouse simulator.
* Particle system in standard asset to make the game more interested to watch.

## Additional User Stories:

1. As a player I should be presented with some coins that are random to make me jump more frequently.
2. As a player I should gain 10 points for every coin I got.
3. As a player I should love to see color changing when I'm playing.

## Game Flow:

1. The game have only one scene and manage by Class GameManager.
2. GameManager: control start, pause, gameover and quit game.
3. BallController: control physical of the ball and auto move the ball.
4. BallInput: control which type of input to control the ball. Can be extend to use button, touch or keyboard
5. InfinitieTrack: control all platforms which have an obstacle on it. Obstacle have 4 type to random.